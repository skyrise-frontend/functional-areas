import http from '@/services/index'

export async function getFunctionalAreas () {
  return await http.get('data.json')
}

export async function editFunctionalArea (data) {
  return await http.put('data.json', data)
}

export async function createFunctionalArea (data) {
  return await http.post('data.json', data)
}

export async function deleteFunctionalArea () {
  return await http.delete('data.json')
}

export default {
  getFunctionalAreas,
  editFunctionalArea,
  createFunctionalArea,
  deleteFunctionalArea
}
