import axios from 'axios'
import httpMethods from './http-methods'

const axiosInstance = axios.create({
  baseURL: process.env.BASE_URL
})

export default {
  ...httpMethods(axiosInstance)
}
