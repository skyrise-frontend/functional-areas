export default function mapHttpMethods (axiosInstance) {
  return {
    get (url) {
      return axiosInstance.get(url, {
        headers: {
          Accept: 'application/json'
        }
      })
    },

    post (url, data) {
      return axiosInstance.post(url, data, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
    },

    put (url, data) {
      return axiosInstance.put(url, data, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
    },

    patch (url, data) {
      return axiosInstance.patch(url, data, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
    },

    delete (url) {
      return axiosInstance.delete(url, {
        headers: {
          Accept: 'application/json'
        }
      })
    }
  }
}
