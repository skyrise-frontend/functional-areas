import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export const FUNCTIONAL_AREAS_ROUTE = 'functional-areas'
export const FUNCTIONAL_AREA_EDIT_ROUTE = 'functional-area-edit'
export const FUNCTIONAL_AREAS_ADD_NEW_ROUTE = 'functional-area-add'
export const FUNCTIONAL_AREAS_DEFAULT_ROUTE = FUNCTIONAL_AREAS_ROUTE

const routes = [
  {
    path: '/',
    name: FUNCTIONAL_AREAS_DEFAULT_ROUTE,
    component: () => import('../views/FunctionalAreas.vue'),
    redirect: { name: FUNCTIONAL_AREAS_ROUTE },
    children: [
      {
        path: 'functional-areas/',
        name: FUNCTIONAL_AREAS_ROUTE,
        component: () => ({ component: import('../views/FunctionalAreas.vue') })
      }
    ]
  },
  {
    path: '/functional-area/edit/:id',
    name: FUNCTIONAL_AREA_EDIT_ROUTE,
    component: () => ({ component: import('../views/FunctionalAreaEdit.vue') })
  },
  {
    path: '/functional-area/add-new',
    name: FUNCTIONAL_AREAS_ADD_NEW_ROUTE,
    component: () => import('../views/FunctionalAreasAddNew.vue')
  },
  { path: '*', redirect: '/' }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
