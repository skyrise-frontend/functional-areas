import { getFunctionalAreas } from '@/services/api'
import { tryCatch } from '@/helpers/error/try-catch'

export default {
  async fetchAreas ({ commit }) {
    const [error, res] = await tryCatch(getFunctionalAreas())
    if (!error) {
      commit('saveAreas', res.data)
    } else {
      commit('saveAreas', [])
    }
  }
}
