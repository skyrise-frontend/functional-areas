export default {
  saveAreas (state, areasList) {
    state.areas = areasList
  },

  updateArea (state, updatedArea) {
    state.areas = state.areas.map(item => (item.id === updatedArea.id) ? updatedArea : item)
  },

  createArea (state, newArea) {
    state.areas.unshift(newArea)
  },

  deleteArea (state, index) {
    state.areas.splice(index, 1)
  }
}
