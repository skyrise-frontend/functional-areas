export default {
  setToast (state, toastSettings) {
    state.toast.show = toastSettings.show
    state.toast.status = toastSettings.status
  }
}
